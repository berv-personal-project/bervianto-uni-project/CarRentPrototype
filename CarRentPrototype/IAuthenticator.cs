﻿namespace CarRentPrototype
{
    interface IAuthenticator
    {
        void Authenticate();
    }
}
