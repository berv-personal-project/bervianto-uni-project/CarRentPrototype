﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CarRentPrototype
{
    public enum OrderStage
    {
        SELECT_CAR_TYPE = 10000,
        SET_CAR_PASSENGER = 10001,
        FILL_ORDER = 10002
    }

    public enum OrderMenu
    {
        ORDER_CAR = 20001,
        MY_ORDERS = 20002,
        MY_INFORMATION = 20003
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //Maintain on what order step customer is.
        private OrderStage orderStage;
        //Maintain what layout group should be viewed given an order step.
        private IDictionary<OrderStage, Grid> orderStageDictionary;
        //Maintain on what menu customer is.
        private OrderMenu orderMenu;
        //Maintain what layout group should be viewed given an order menu.
        private IDictionary<OrderMenu, Grid> orderMenuDictionary;

        public MainWindow()
        {
            InitializeComponent();
            InitializeProperty();
        }

        /// <summary>
        /// Initialize this class additional property.
        /// </summary>
        private void InitializeProperty()
        {
            orderStageDictionary = new Dictionary<OrderStage, Grid>();
            orderStageDictionary.Add(OrderStage.SELECT_CAR_TYPE, SelectCarTypePage);
            orderStageDictionary.Add(OrderStage.SET_CAR_PASSENGER, SelectCarPassengerPage);
            orderStageDictionary.Add(OrderStage.FILL_ORDER, FillInformationPage);

            orderMenuDictionary = new Dictionary<OrderMenu, Grid>();
            orderMenuDictionary.Add(OrderMenu.ORDER_CAR, CarOrderMenu);
            orderMenuDictionary.Add(OrderMenu.MY_ORDERS, MyOrderMenu);
            orderMenuDictionary.Add(OrderMenu.MY_INFORMATION, MyInformationMenu);

            orderStage = OrderStage.SELECT_CAR_TYPE;
            orderMenu = OrderMenu.ORDER_CAR;
            RefreshOrderView();
        }

        /// <summary>
        /// Get maximum value of OrderStage enumeration
        /// </summary>
        /// <returns> maximum value of order stage enum </returns>
        private int OrderStageMaxValue()
        {
            return (int)OrderStage.FILL_ORDER;
        }

        /// <summary>
        /// Get maximum value of OrderStage enumeration
        /// </summary>
        /// <returns> maximum value of order stage enum </returns>
        private int OrderStageMinValue()
        {
            return (int)OrderStage.SELECT_CAR_TYPE;
        }

        /// <summary>
        /// Handle Next Step Order Click Event. Will make order page displays next order step
        /// </summary>
        /// <param name="sender"> not used </param>
        /// <param name="e">not used</param>
        private void HandleNextOrderStep(object sender, RoutedEventArgs e)
        {
            int orderStageInt = (int)orderStage;
            orderStage = (OrderStage) Math.Min(OrderStageMaxValue(), orderStageInt + 1);
            RefreshOrderView();
        }

        /// <summary>
        /// Handle previous step order click event. Will make order page displays previous order step
        /// </summary>
        /// <param name="sender">not used</param>
        /// <param name="e">not used</param>
        private void HandlePreviousOrderStep(object sender, RoutedEventArgs e)
        {
            int orderStageInt = (int)orderStage;
            orderStage = (OrderStage)Math.Max(OrderStageMinValue(), orderStageInt - 1);
            RefreshOrderView();
        }

        /// <summary>
        /// Refresh order view. Grid correspond to orderStage will be set to visible, while others will be set to hidden
        /// </summary>
        private void RefreshOrderView()
        {
            foreach (KeyValuePair<OrderMenu, Grid> element in orderMenuDictionary)
            {
                if (element.Key == orderMenu)
                {
                    element.Value.Visibility = Visibility.Visible;
                }
                else
                {
                    element.Value.Visibility = Visibility.Hidden;
                }
            }
            foreach (KeyValuePair<OrderStage, Grid> element in orderStageDictionary)
            {
                if (element.Key == orderStage)
                {
                    element.Value.Visibility = Visibility.Visible;
                }
                else
                {
                    element.Value.Visibility = Visibility.Hidden;
                }
            }
        }

        /// <summary>
        /// View Order Car Menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewOrderCarMenu(object sender, RoutedEventArgs e)
        {
            orderMenu = OrderMenu.ORDER_CAR;
            RefreshOrderView();
        }

        /// <summary>
        /// View My Orders Menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewMyOrdersMenu(object sender, RoutedEventArgs e)
        {
            //orderMenu = OrderMenu.MY_ORDERS;
            RefreshOrderView();
        }

        /// <summary>
        /// View My Informations Menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewMyInformationMenu(object sender, RoutedEventArgs e)
        {
            orderMenu = OrderMenu.MY_INFORMATION;
            RefreshOrderView();
        }

        private void HandleTypeSelect(object sender, RoutedEventArgs e)
        {
            HandleNextOrderStep(sender, e);
        }
    }
}
