﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRentPrototype
{
    interface IShowable
    {
        void Show();
        void Hide();
        void OnDraw();
        void OnHide();
    }
}
